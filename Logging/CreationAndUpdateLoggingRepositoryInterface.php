<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging;

use JohnSear\JspLoggingEntityBundle\Logging\Traits\RepositoryCreationInterface;
use JohnSear\JspLoggingEntityBundle\Logging\Traits\RepositoryUpdateInterface;

interface CreationAndUpdateLoggingRepositoryInterface extends RepositoryCreationInterface, RepositoryUpdateInterface
{

}
