<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging;

use JohnSear\JspLoggingEntityBundle\Logging\Base\AbstractLoggingEntity;
use JohnSear\JspLoggingEntityBundle\Logging\Traits\EntityCreationTrait;
use JohnSear\JspLoggingEntityBundle\Logging\Traits\EntityUpdateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 */
abstract class AbstractCreationAndUpdateLoggingEntity extends AbstractLoggingEntity
    implements CreationAndUpdateLoggingEntityInterface
{
    use EntityCreationTrait;
    use EntityUpdateTrait;
}
