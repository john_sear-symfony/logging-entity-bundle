<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging;

use JohnSear\JspLoggingEntityBundle\Logging\Base\AbstractLoggingRepository;
use JohnSear\JspLoggingEntityBundle\Logging\Traits\RepositoryCreationTrait;
use JohnSear\JspLoggingEntityBundle\Logging\Traits\RepositoryUpdateTrait;

abstract class AbstractCreationAndUpdateLoggingRepository extends AbstractLoggingRepository
    implements CreationAndUpdateLoggingRepositoryInterface
{
    use RepositoryCreationTrait;
    use RepositoryUpdateTrait;
}
