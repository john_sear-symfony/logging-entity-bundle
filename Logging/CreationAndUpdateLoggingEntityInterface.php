<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging;

use JohnSear\JspLoggingEntityBundle\Logging\Traits\EntityCreationInterface;
use JohnSear\JspLoggingEntityBundle\Logging\Traits\EntityUpdateInterface;

interface CreationAndUpdateLoggingEntityInterface extends EntityCreationInterface, EntityUpdateInterface
{

}
