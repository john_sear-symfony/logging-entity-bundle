<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging\Base;

use Symfony\Component\Security\Core\User\UserInterface;

interface LoggingEntityInterface
{
    public function getCurrentUser(): ?UserInterface;
    public function setCurrentUser(UserInterface $user): LoggingEntityInterface;
}
