<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging\Base;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use JohnSear\JspUserBundle\Exception\NotAuthenticatedException;
use JohnSear\JspUserBundle\Exception\NoValidUserAuthenticatedException;
use JohnSear\JspUserBundle\UserResolver\UserResolverInterface;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractLoggingRepository extends ServiceEntityRepository
    implements LoggingRepositoryInterface
{
    private $userResolver;

    public function __construct(
        ManagerRegistry $registry,
        UserResolverInterface $userResolver
    )
    {
        parent::__construct($registry, $this->getEntityClass());
        $this->userResolver = $userResolver;
    }

    abstract public function getEntityClass(): string;

    /**
     * @throws NoValidUserAuthenticatedException
     * @throws NotAuthenticatedException
     */
    public function setCurrentUserIfNull(LoggingEntityInterface $entity): void
    {
        if (!$entity->getCurrentUser() instanceof UserInterface) {
            $entity->setCurrentUser($this->userResolver->getCurrentUser());
        }
    }
}
