<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging\Base;

use JohnSear\JspUserBundle\Exception\NotAuthenticatedException;
use JohnSear\JspUserBundle\Exception\NoValidUserAuthenticatedException;

interface LoggingRepositoryInterface
{
    public function getEntityClass(): string;

    /**
     * @throws NoValidUserAuthenticatedException
     * @throws NotAuthenticatedException
     */
    public function setCurrentUserIfNull(LoggingEntityInterface $entity): void;
}
