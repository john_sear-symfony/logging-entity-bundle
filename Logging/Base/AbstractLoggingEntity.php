<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging\Base;

use JohnSear\Utilities\Uuid\Entity\AbstractUuidEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 */
abstract class AbstractLoggingEntity extends AbstractUuidEntity implements LoggingEntityInterface
{
    private $currentUser;

    public function getCurrentUser(): ?UserInterface
    {
        return $this->currentUser;
    }

    public function setCurrentUser(UserInterface $user): LoggingEntityInterface
    {
        $this->currentUser = $user;

        return $this;
    }
}
