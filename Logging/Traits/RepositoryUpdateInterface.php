<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging\Traits;

use JohnSear\JspLoggingEntityBundle\Logging\Base\LoggingEntityInterface;

interface RepositoryUpdateInterface
{
    public function beforeUpdate(LoggingEntityInterface $entity): void;
}
