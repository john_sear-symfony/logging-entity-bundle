<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging\Traits;

use JohnSear\JspLoggingEntityBundle\Logging\Base\LoggingEntityInterface;
use JohnSear\JspLoggingEntityBundle\Logging\Base\LoggingRepositoryInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 */
trait RepositoryCreationTrait
{
    public function beforeInsert(LoggingEntityInterface $entity): void
    {
        /** @var LoggingRepositoryInterface $this */
        /** @var LoggingEntityInterface $entity */
        $this->setCurrentUserIfNull($entity);
    }
}
