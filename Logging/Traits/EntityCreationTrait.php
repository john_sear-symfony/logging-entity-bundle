<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging\Traits;

use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use JohnSear\JspLoggingEntityBundle\Logging\Base\LoggingEntityInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\HasLifecycleCallbacks()
 */
trait EntityCreationTrait
{
    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="JohnSear\JspUserBundle\Entity\User")
     */
    protected $createdBy;

    public function getCreatedAt(): ? DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): EntityCreationInterface
    {
        $this->createdAt = $createdAt;

        /** @var EntityCreationInterface $this */
        return $this;
    }

    public function getCreatedBy(): ? UserInterface
    {
        return $this->createdBy;
    }

    public function setCreatedBy(UserInterface $user): EntityCreationInterface
    {
        $this->createdBy = $user;

        /** @var EntityCreationInterface $this */
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValueOnCreation(LifecycleEventArgs $eventArgs): EntityCreationInterface
    {
        /** @var EntityCreationInterface $entity */
        $entity = $eventArgs->getEntity();

        $now = (new DateTime('now'))->setTimezone(new \DateTimeZone('Europe/Berlin'));

        return $entity->setCreatedAt($now);
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedByValueOnCreation(LifecycleEventArgs $eventArgs): EntityCreationInterface
    {
        /** @var EntityCreationInterface $entity */
        $entity = $eventArgs->getEntity();

        /** @var LoggingEntityInterface $userLoggingEntity */
        $userLoggingEntity = $entity;

        /** @var RepositoryCreationInterface $loggingRepository */
        $loggingRepository = $eventArgs->getEntityManager()->getRepository(get_class($entity));

        $loggingRepository->beforeInsert($userLoggingEntity);

        return $entity->setCreatedBy($userLoggingEntity->getCurrentUser());
    }
}
