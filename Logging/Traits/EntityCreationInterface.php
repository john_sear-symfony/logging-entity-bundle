<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging\Traits;

use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\User\UserInterface;

interface EntityCreationInterface
{
    public function getCreatedAt(): ? DateTime;
    public function setCreatedAt(DateTime $createdAt): EntityCreationInterface;

    public function getCreatedBy(): ? UserInterface;
    public function setCreatedBy(UserInterface $user): EntityCreationInterface;

    public function setCreatedAtValueOnCreation(LifecycleEventArgs $eventArgs): EntityCreationInterface;

    public function setCreatedByValueOnCreation(LifecycleEventArgs $eventArgs): EntityCreationInterface;
}
