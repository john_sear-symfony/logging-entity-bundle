<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging\Traits;

use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\User\UserInterface;

interface EntityUpdateInterface
{
    public function getUpdatedAt(): ? DateTime;
    public function setUpdatedAt(DateTime $updatedAt): EntityUpdateInterface;

    public function getUpdatedBy(): ? UserInterface;
    public function setUpdatedBy(UserInterface $user): EntityUpdateInterface;

    public function setUpdatedByValueOnCreation(LifecycleEventArgs $eventArgs): EntityUpdateInterface;
    public function setUpdatedByValueOnUpdate(PreUpdateEventArgs $eventArgs): EntityUpdateInterface;

    public function setUpdatedAtValueOnCreation(LifecycleEventArgs $eventArgs): EntityUpdateInterface;
    public function setUpdatedAtValueOnUpdate(PreUpdateEventArgs $eventArgs): EntityUpdateInterface;
}
