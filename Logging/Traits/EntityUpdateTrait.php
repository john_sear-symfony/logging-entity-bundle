<?php declare(strict_types=1);


namespace JohnSear\JspLoggingEntityBundle\Logging\Traits;

use DateTime;
use DateTimeZone;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use JohnSear\JspLoggingEntityBundle\Logging\Base\LoggingEntityInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\HasLifecycleCallbacks()
 */
trait EntityUpdateTrait
{
    /**
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="JohnSear\JspUserBundle\Entity\User")
     */
    protected $updatedBy;

    public function getUpdatedAt(): ? DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt): EntityUpdateInterface
    {
        $this->updatedAt = $updatedAt;

        /** @var EntityUpdateInterface $this */
        return $this;
    }

    public function getUpdatedBy(): ? UserInterface
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(UserInterface $user): EntityUpdateInterface
    {
        $this->updatedBy = $user;

        /** @var EntityUpdateInterface $this */
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setUpdatedAtValueOnCreation(LifecycleEventArgs $eventArgs): EntityUpdateInterface
    {
        return $this->setUpdatedAtValue($eventArgs);
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValueOnUpdate(PreUpdateEventArgs $eventArgs): EntityUpdateInterface
    {
        return $this->setUpdatedAtValue($eventArgs);
    }

    private function setUpdatedAtValue(LifecycleEventArgs $eventArgs): EntityUpdateInterface
    {
        /** @var EntityUpdateInterface $entity */
        $entity = $eventArgs->getEntity();

        $now = (new DateTime('now'))->setTimezone(new DateTimeZone('Europe/Berlin'));

        return $entity->setUpdatedAt($now);
    }

    /**
     * @ORM\PrePersist
     */
    public function setUpdatedByValueOnCreation(LifecycleEventArgs $eventArgs): EntityUpdateInterface
    {
        return $this->setUpdatedByValue($eventArgs);
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedByValueOnUpdate(PreUpdateEventArgs $eventArgs): EntityUpdateInterface
    {
        return $this->setUpdatedByValue($eventArgs);
    }

    private function setUpdatedByValue(LifecycleEventArgs $eventArgs): EntityUpdateInterface
    {
        /** @var EntityUpdateInterface $entity */
        $entity = $eventArgs->getEntity();

        /** @var LoggingEntityInterface $userLoggingEntity */
        $userLoggingEntity = $entity;

        /** @var RepositoryUpdateInterface $loggingRepository */
        $loggingRepository = $eventArgs->getEntityManager()->getRepository(get_class($entity));

        $loggingRepository->beforeUpdate($userLoggingEntity);

        return $entity->setUpdatedBy($userLoggingEntity->getCurrentUser());
    }
}
