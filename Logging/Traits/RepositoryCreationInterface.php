<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\Logging\Traits;

use JohnSear\JspLoggingEntityBundle\Logging\Base\LoggingEntityInterface;

interface RepositoryCreationInterface
{
    public function beforeInsert(LoggingEntityInterface $entity): void;
}
