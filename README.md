# Logging Entity Bundle
(c) 2019 by [John_Sear](https://bitbucket.org/john_sear-symfony/logging-entity-bundle/)

## Symfony Bundle

This is a Symfony Bundle to log Entity Activity in your symfony application.  
Be sure you are using symfony version 4.4.

> This Bundle is still in Development. Some things can be broken ;-)

## Installation

### via CLI
Run ``composer require jsp/logging-entity-bundle:"0.1.*"`` command in cli to install source starting from Version 0.1.5

### composer.json
Add following to your symfony application composer json file:
```json
{
  "require": {
    "jsp/logging-entity-bundle": "0.1.*"
  }
}
```

## Configuration

### Register all Services

For now, all services must be auto wired via the services.yaml.

Add following Lines at the end of ``config\services.yaml``

```yaml
services:

    # ..

    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones

    JohnSear\JspLoggingEntityBundle\:
        resource: '../vendor/jsp/logging-entity-bundle/*'
# ..
```

### Configure the Doctrine Target Entity Listener

```yaml
# config/packages/doctrine.yaml
doctrine:
    # ...
    orm:
        # ...
        resolve_target_entities:
            Symfony\Component\Security\Core\User\UserInterface: JohnSear\JspLoggingEntityBundle\Logging\Base\LoggingEntityInterface
```
