<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle;

use JohnSear\JspLoggingEntityBundle\DependencyInjection\JspLoggingEntityExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class JspLoggingEntityBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new JspLoggingEntityExtension();
    }
}
