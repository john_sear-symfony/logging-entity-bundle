<?php declare(strict_types=1);

namespace JohnSear\JspLoggingEntityBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class JspLoggingEntityExtension extends Extension
{
    /** @var ContainerBuilder */
    private $container;

    /**
     * @throws Exception
     */
    public function load(array $config, ContainerBuilder $container)
    {
        $this->container = $container;

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}
